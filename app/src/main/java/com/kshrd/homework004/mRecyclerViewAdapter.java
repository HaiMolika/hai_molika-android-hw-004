package com.kshrd.homework004;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class mRecyclerViewAdapter extends RecyclerView.Adapter<mRecyclerViewAdapter.ViewHolder> {

    private String[] email_list;

    public mRecyclerViewAdapter(String[] email_list){
        this.email_list = email_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.view_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(email_list[position]);
    }

    @Override
    public int getItemCount() {
        return email_list.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        View view;
        public ViewHolder(View itemView){
            super(itemView);
            textView = itemView.findViewById(R.id.txtEmail);
            view = itemView.findViewById(R.id.lines);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(),email_list[getAdapterPosition()], Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

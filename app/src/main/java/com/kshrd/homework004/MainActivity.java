package com.kshrd.homework004;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements FragmentActionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(findViewById(R.id.activity_portrait) != null){ ;
            makeFragmentTransaction(new HomeFragment(), false, getResources().getString(R.string.home_fragment));
        }else if(findViewById(R.id.activity_landscape) != null){
            makeFragmentTransaction(R.id.fragmentcontainer, new HomeFragment(), false, getResources().getString(R.string.list_fragment));
            makeFragmentTransaction(R.id.detailfragment, new ListFragment(), false, getResources().getString(R.string.list_fragment));
        }
    }

    private void makeFragmentTransaction(Fragment fragment, boolean addToBackStack, String Tag){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentcontainer, fragment, Tag);
        if(addToBackStack){
            transaction.addToBackStack(Tag);
        }
        transaction.commit();
    }

    private void makeFragmentTransaction(int containerID, Fragment fragment, boolean addToBackStack, String Tag){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(containerID, fragment, Tag);
        if(addToBackStack){
            transaction.addToBackStack(Tag);
        }
        transaction.commit();
    }

    @Override
    public void onButtonClick() {
        makeFragmentTransaction(new ListFragment(), true, getResources().getString(R.string.list_fragment));
    }
}
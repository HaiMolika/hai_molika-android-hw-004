package com.kshrd.homework004;

public interface FragmentActionListener {

    public void onButtonClick();
}
